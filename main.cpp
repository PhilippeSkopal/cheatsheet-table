#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include <algorithm>

using namespace std;
//prototype de ma fonction
void PrintArray(int * TabPointer, int SizeTab);

int main()
{
    srand(time(NULL));

    // Cr�ation du tableau

    int tab[5];                                             // Cr�er un tableau de 5 cases

    //Remplissage du tableau
    unsigned i;
    for (i = 0 ; i < (sizeof(tab)/sizeof(int)) ; i++){      // (sizeof(tab)/sizeof(int)) ==> Trouve la taille du tableau, ici renvois 5
        tab[i] = rand()%10+1;                               // Remplis avec de 1 � 10
    }

    //Trier le tableau sans effort
    std::sort(tab, (tab + sizeof(tab)/sizeof(int))-1);

    PrintArray(tab, 5);

    return 0;
}

void PrintArray(int * TabPointer, int SizeTab)
{
    // Afficher le tableau
    int i;
    for (i = 0 ; i < SizeTab ; i++){
        cout << TabPointer[i]  << endl;
    }
}
